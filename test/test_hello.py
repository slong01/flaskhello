'Tests for flaskhello'


import pytest
import flaskhello


def test_hello():
    'Ensure we get the right output from hello'
    assert flaskhello.hello('Lancelot') == 'hello, Lancelot!'
