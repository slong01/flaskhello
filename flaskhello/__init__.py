'Simple Flask hello application'


from flask import Flask


app = Flask(__name__)


@app.route('/')
def root():
    'Return a simple message instructing the user'
    return 'Try /hello/name'


@app.route('/hello/<name>')
def hello(name):
    'Return a hello with the name from the URL'
    return 'hello, %s!' % name


def main():
    'Run the flask application'
    app.run()
