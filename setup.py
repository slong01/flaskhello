from setuptools import setup

setup(name='flaskhello',
      version='0.0.1',
      author='Eric Chai',
      author_email='electromatter@gmail.com',
      description='A very minimal demo app for flask',
      license='ISC',
      packages=['flaskhello'],
      install_requires=['flask'],
      zip_safe=True)
