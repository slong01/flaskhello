FROM python:3-alpine 
COPY dist/* dist/
RUN python3 -m pip install gunicorn==19.9.0 /dist/*.whl && rm -r /dist
CMD gunicorn -b :4000 flaskhello:app
EXPOSE 4000

